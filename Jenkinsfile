pipeline {
	agent {
        kubernetes {
            label 'centos-latest'
        }
    }
    
	parameters {
        choice choices: ['ci', 'integration', 'milestone', 'release'],
            description: 'Sets the build type. CI builds are used for testing reasons only; integration builds are periodic builds; milestones are considered more-or-less stable, while releases are Eclipse.org releases with a corresponding release review.',
            name: 'BUILD_TYPE'
        string defaultValue: '',
            description: 'If set, the tycho-versions plugin is used to update the versions of the Maven plugins before deployment for graduated plugins',
            name: 'RELEASE_VERSION'
	}
	options {
		buildDiscarder(logRotator(numToKeepStr: '5'))
        timeout(120 /*minutes*/) 
	}
	environment {
	   VERSION_MAVEN_PARAMETER = " ${params.RELEASE_VERSION ? '-Drepository.version=' + params.RELEASE_VERSION : ''} "
	   SIGN_BUILD_PARAMETER = " ${params.BUILD_TYPE == 'ci' ? '' : '-P eclipse-sign'} "
	}
	
	tools {
        maven 'apache-maven-latest'
        jdk 'temurin-jdk11-latest'
    }

	stages {
		stage('Full build') { 
			steps {
                xvnc {
                    sh "mvn -B -f releng/org.eclipse.sphinx.releng.builds/pom.xml ${SIGN_BUILD_PARAMETER} ${VERSION_MAVEN_PARAMETER} -Dmaven.repo.local=$WORKSPACE/.repository -Dmaven.test.failure.ignore=true clean install"
                }
			}
		}
		stage('download.eclipse.org') {
		  when {expression { return params.BUILD_TYPE != "ci" }}
		  steps {
		      sshagent ( ['projects-storage.eclipse.org-bot-ssh']) {
		          sh '''
                        export PROJECT=$WORKSPACE/releng/org.eclipse.sphinx.releng.builds/repository
                        export WORK_DIR=$PROJECT/work
                        export BUILD_DIR=/home/data/httpd/download.eclipse.org/sphinx/updates/$BUILD_TYPE
                        
                        export VERSION=$(<$PROJECT/target/classes/version)
                        mkdir -p $WORK_DIR
                        
                        # Upload repository to download.eclipse.org
                        ssh -o BatchMode=yes genie.sphinx@projects-storage.eclipse.org mkdir -p $BUILD_DIR/$VERSION
                        scp -o BatchMode=yes -r $PROJECT/target/repository/* genie.sphinx@projects-storage.eclipse.org:$BUILD_DIR/$VERSION
		          '''
		      }
		  }
		}
	}

	post {
		always {
			archiveArtifacts artifacts: 'releng/org.eclipse.sphinx.releng.builds/repository/target/repository/**'
            archiveArtifacts artifacts: 'releng/org.eclipse.sphinx.releng.builds/repository/target/org.eclipse.sphinx.updates-*.zip'
			junit testResults: '**/tests/**/target/surefire-reports/*.xml'
		}
    }
}