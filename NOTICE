# Notices for Eclipse Sphinx

This content is produced and maintained by the Eclipse Sphinx project.

* Project home: https://projects.eclipse.org/projects/automotive.sphinx

## Trademarks

Eclipse Sphinx, and Sphinx are trademarks of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v2.0 which is available at
https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://git.eclipse.org/r/plugins/gitiles/sphinx/org.eclipse.sphinx

## Third-party Content

This project leverages the following third party content.

ANTLR Runtime only: (3.2)

* License: New BSD License

Apache Commons Command Line Interface CLI (1.2)

* License: Apache License, 2.0

Apache Commons Lang (2.4)

* License: Apache License, 2.0

Apache Commons Lang (2.6)

* License: Apache License, 2.0

Apache Commons Lang (3.1.0)

* License: Apache License 2.0

Apache Commons Line Interface (CLI) (1.0)

* License: Apache Software License 1.1

Apache Commons Logging Jar (1.1.1)

* License: Apache License, 2.0

Google Guava (15.0.0)

* License: Apache License 2.0

Google Guava (21.0)

* License: Apache License, 2.0

Guava (10.0.1)

* License: Apache License, 2.0

Guava (12.0)

* License: Apache License 2.0

JDom (1.0)

* License: JDom License (based on Apache 1.1 Style License)

JDom (1.1.1)

* License: JDom License (based on Apache 1.1 Style License)

jQuery (Includes Sizzle Component) (1.11.1)

* License: MIT license

resolver.jar (1.2)

* License: Apache License, 2.0

serializer.jar (2.7.1)

* License: Apache License, 2.0

Twitter Bootstrap (3.1.1)

* License: MIT License

Xerces (2.9.0)

* License: Apache License, 2.0

xml-apis.jar (1.3.04)

* License: Apache License, 2.0, Public Domain, W3C

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.