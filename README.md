# Eclipse Sphinx

Eclipse Sphinx provide a modeling tool platform for Eclipse that eases the development of IDE-like tool support for modeling languages used in software and systems development.

You can find more information on the [official website](http://www.eclipse.org/sphinx/) or the [wiki](https://wiki.eclipse.org/Sphinx).

