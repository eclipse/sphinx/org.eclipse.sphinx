/**
 * <copyright>
 * 
 * Copyright (c) 2013 itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 * 
 * Contributors: 
 *     itemis - Initial API and implementation
 * 
 * </copyright>
 */
package org.eclipse.sphinx.examples.hummingbird.ide.ui.internal.preferences;

public interface IHummingbirdPreferencesUI {

	/**
	 * The ID of the Hummingbird metamodel version preference page.
	 */
	public static final String HUMMINGBIRD_METAMODEL_VERSION_PREFERENCE_PAGE_ID = "org.eclipse.sphinx.examples.hummingbird.ide.ui.preferencePages.hummingbirdMetaModelVersion"; //$NON-NLS-1$
}
