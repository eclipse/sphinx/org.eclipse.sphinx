/**
 * <copyright>
 *
 * Copyright (c) 2008-2010 See4sys and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *     See4sys - Initial API and implementation
 *
 * </copyright>
 */
package org.eclipse.sphinx.examples.hummingbird.metamodelgen.ui;

import org.eclipse.sphinx.examples.hummingbird.metamodelgen.ui.internal.messages.Messages;

/**
 * Defines constants for Hummingbird metamodel generation example menus and groups.
 *
 * @since 0.8.0
 */
public interface IHummingbirdMetaModelGenExampleMenuConstants {

	/**
	 * Identifier of the metamodel generation sub menu.
	 */
	String MENU_META_MODEL_GEN_ID = "hummingbird.metamodelgen.examples.menu"; //$NON-NLS-1$

	/**
	 * Label of the metamodel generation sub menu.
	 */
	String MENU_META_MODEL_GEN_LABEL = Messages.menu_metaModelGen_label;
}
