/**
 * <copyright>
 *
 * Copyright (c) 2015 itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *     itemis - Initial API and implementation
 *
 * </copyright>
 */
package org.eclipse.sphinx.examples.hummingbird20.splitting.ui;

import org.eclipse.sphinx.examples.hummingbird20.splitting.ui.internal.messages.Messages;

public interface IHummingbirdSplittingExampleMenuConstants {

	/**
	 * Identifier of the Model Splitting sub menu.
	 */
	public static final String MENU_MODEL_SPLITTING_ID = "hummingbird.splitting.examples.menu"; //$NON-NLS-1$

	/**
	 * Label of the Model Splitting sub menu.
	 */
	public static final String MENU_MODEL_SPLITTING_LABEL = Messages.menu_modelSplitting_label;
}
