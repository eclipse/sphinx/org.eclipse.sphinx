/**
 * <copyright>
 * 
 * Copyright (c) 2008-2014 See4sys, itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 * 
 * Contributors: 
 *     See4sys - Initial API and implementation
 *     itemis - Enhancements and maintenance
 * 
 * </copyright>
 */
package org.eclipse.sphinx.examples.hummingbird20.instancemodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Custom Application</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.sphinx.examples.hummingbird20.instancemodel.InstanceModel20Package#getCustomApplication()
 * @model
 * @generated
 */
public interface CustomApplication extends Application {
} // CustomApplication
