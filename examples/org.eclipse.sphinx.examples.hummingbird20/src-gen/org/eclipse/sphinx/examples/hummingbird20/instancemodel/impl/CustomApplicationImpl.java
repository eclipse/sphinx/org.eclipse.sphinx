/**
 * <copyright>
 * 
 * Copyright (c) 2008-2014 See4sys, itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 * 
 * Contributors: 
 *     See4sys - Initial API and implementation
 *     itemis - Enhancements and maintenance
 * 
 * </copyright>
 */
package org.eclipse.sphinx.examples.hummingbird20.instancemodel.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.sphinx.examples.hummingbird20.instancemodel.CustomApplication;
import org.eclipse.sphinx.examples.hummingbird20.instancemodel.InstanceModel20Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Custom Application</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CustomApplicationImpl extends ApplicationImpl implements CustomApplication {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CustomApplicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return InstanceModel20Package.Literals.CUSTOM_APPLICATION;
	}

}
