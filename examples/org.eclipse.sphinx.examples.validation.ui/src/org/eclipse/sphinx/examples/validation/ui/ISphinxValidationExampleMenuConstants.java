/**
 * <copyright>
 * 
 * Copyright (c) 2008-2010 See4sys and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 * 
 * Contributors: 
 *     See4sys - Initial API and implementation
 * 
 * </copyright>
 */
package org.eclipse.sphinx.examples.validation.ui;

import org.eclipse.sphinx.examples.validation.ui.internal.messages.Messages;

/**
 * Defines constants for Sphinx validation example menus and groups.
 * 
 * @since 0.7.0
 */
public interface ISphinxValidationExampleMenuConstants {

	/**
	 * Identifier of the validation sub menu.
	 */
	String MENU_VALIDATION_ID = "sphinx.validation.examples.menu"; //$NON-NLS-1$

	/**
	 * Label of the validation sub menu.
	 */
	String MENU_VALIDATION_LABEL = Messages.menu_validation_label;
}
