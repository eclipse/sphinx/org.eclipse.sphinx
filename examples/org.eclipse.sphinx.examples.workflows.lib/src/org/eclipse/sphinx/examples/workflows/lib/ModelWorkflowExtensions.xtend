/**
 * <copyright>
 *
 * Copyright (c) 2014-2016 itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *     itemis - Initial API and implementation
 *     itemis - [503063] Provide launching support for Sphinx Workflows
 *
 * </copyright>
 */
package org.eclipse.sphinx.examples.workflows.lib

import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.edit.provider.AdapterFactoryItemDelegator
import org.eclipse.emf.edit.provider.ComposedAdapterFactory
import org.eclipse.emf.mwe.core.WorkflowContext
import org.eclipse.sphinx.emf.mwe.dynamic.IModelWorkflowSlots

class ModelWorkflowExtensions {

	def static List<EObject> getModelSlot(WorkflowContext ctx) {
		ctx.get(IModelWorkflowSlots.MODEL_SLOT_NAME) as List<EObject>
	}

	def static Map<String, Object> getArgumentsSlot(WorkflowContext ctx) {
		ctx.get(IModelWorkflowSlots.ARGUMENTS_SLOT_NAME) as Map<String, Object>
	}

	def static String getLabel(EObject modelObject) {
		val adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		val delegator = new AdapterFactoryItemDelegator(adapterFactory)
		val label = delegator.getText(modelObject)
		if (label != null && !label.empty) {
			return label
		} else {
			return modelObject.eClass.name + " <Unnamed>"
		}
	}
}
