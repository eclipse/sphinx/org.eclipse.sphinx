/**
 * <copyright>
 *
 * Copyright (c) 2014 itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *     itemis - Initial API and implementation
 *
 * </copyright>
 */
package org.eclipse.sphinx.emf.check.ui.internal;

public class CheckValidationImageProvider {

	public static final String FILE = "icons/model.png"; //$NON-NLS-1$
	public static final String ERROR_ICO = "icons/error.png"; //$NON-NLS-1$
	public static final String WARNING_ICO = "icons/warning.png"; //$NON-NLS-1$
	public static final String INFO_ICO = "icons/info.png"; //$NON-NLS-1$
	public static final String GROUP_ICO = "icons/group.png"; //$NON-NLS-1$
	public static final String CHECK_ICO = "icons/check.png"; //$NON-NLS-1$
}
