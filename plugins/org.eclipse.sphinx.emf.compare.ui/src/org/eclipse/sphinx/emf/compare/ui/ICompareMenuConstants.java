/**
 * <copyright>
 *
 * Copyright (c) 2008-2015 See4sys, itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *     See4sys - Initial API and implementation
 *     itemis - [457704] Integrate EMF compare 3.x in Sphinx
 *
 * </copyright>
 */
package org.eclipse.sphinx.emf.compare.ui;

public interface ICompareMenuConstants {

	String MENU_COMPARE_WITH_ID = "compareWithMenu";//$NON-NLS-1$
	String MENU_COMPARE_WITH_LABEL = "Compare With"; //$NON-NLS-1$
	String MENU_COMPARE_WITH_GROUP = "compareWithGroup"; //$NON-NLS-1$
}
