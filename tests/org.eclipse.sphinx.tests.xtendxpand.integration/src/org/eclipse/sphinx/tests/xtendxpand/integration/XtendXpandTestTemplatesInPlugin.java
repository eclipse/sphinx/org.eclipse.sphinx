/**
 * <copyright>
 *
 * Copyright (c) 2013  itemis and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *     itemis - Initial API and implementation
 *
 * </copyright>
 */
package org.eclipse.sphinx.tests.xtendxpand.integration;

public interface XtendXpandTestTemplatesInPlugin {

	String CONFIGH_XPT_FILE_PATH = "templates/ConfigH-plugin.xpt"; //$NON-NLS-1$

	String XPAND_CONFIGH_DEFINITION_NAME = "templates::ConfigH-plugin::main"; //$NON-NLS-1$
}
