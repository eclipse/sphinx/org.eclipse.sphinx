
/** 
 * @file    Config.h
 * @version $Revision: 1.1 $, $Date: 2007/05/21 08:54:08 $
 * 
 * @brief   System-related options and parameter settings, and inline 
 *          invocation targets
 */

#ifndef _CONFIG_H
#define _CONFIG_H

// Parameters of component MyComp1
#define ParamVal1 111
#define ParamVal2 222

// Parameters of component MyComp2
#define ParamVal3 333

#endif /* _CONFIG_H */
